using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Walid.Extension
{
    [Serializable]
    public class ThreeDimension
    {
        public float x, y, z;
        public ThreeDimension()
        {
            x = y = z = 0;
        }
        public ThreeDimension(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public ThreeDimension(Vector3 vector3)
        {
            x = vector3.x;
            y = vector3.y;
            z = vector3.z;
        }
    }

    public enum Direction2D
    {
        top,
        bottom,
        left,
        right
    }

    public static class Extension
    {
        /// <summary>
        /// To return on Child Component Except Parent Object
        /// </summary>
        /// <param name="transform">Parent Transform which child we want to get</param>
        /// <returns></returns>
        public static T[] GetComponentsInChildren<T>(this Component component, bool includeInactive, bool exceptParent)
        {
            List<T> components = new List<T>();
            component.GetComponentsInChildren<T>(includeInactive: includeInactive, result: components);
            if (exceptParent) components.Remove(item: component.GetComponent<T>());
            return components.ToArray();
        }

        public static Vector3 ToVector3xyz(this ThreeDimension threeDimension)
        {
            return new Vector3(threeDimension.x, threeDimension.y, threeDimension.z);
        }
        public static Vector3 ToVector3xyz(this ThreeDimension threeDimension, bool setToOrigin)
        {
            return setToOrigin ? new Vector3(0f, threeDimension.y, 0f) : new Vector3(threeDimension.x, threeDimension.y, threeDimension.z);
        }
        /// <summary>
        /// This ScrollRect scrollRect is extension of Scroll React Object
        /// direction to scroll the Scroll React
        /// </summary>
        /// <param name="scrollRect">The ScrollRect we want to Auto Scroll</param>
        /// <param name="direction">The Direction2D we want to scrolling</param>
        /// <returns></returns>
        public static IEnumerator AutoScroll(this ScrollRect scrollRect, Direction2D direction)
        {
            for (float i = 0f; i < 1; i += Time.deltaTime) //i is compared with 1 Because of calculating Frame Rate in 1 second
            {
                switch (direction)
                {
                    case Direction2D.top:
                        scrollRect.verticalNormalizedPosition = Mathf.Lerp(scrollRect.verticalNormalizedPosition, 1f, i);
                        break;
                    case Direction2D.bottom:
                        scrollRect.verticalNormalizedPosition = Mathf.Lerp(scrollRect.verticalNormalizedPosition, 0f, i);
                        break;
                    case Direction2D.left:
                        scrollRect.horizontalNormalizedPosition = Mathf.Lerp(scrollRect.horizontalNormalizedPosition, 1f, i);
                        break;
                    case Direction2D.right:
                        scrollRect.horizontalNormalizedPosition = Mathf.Lerp(scrollRect.horizontalNormalizedPosition, 0f, i);
                        break;
                    default:
                        break;
                }
                yield return new WaitForEndOfFrame();
            }
        }
    }
}